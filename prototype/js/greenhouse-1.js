// >>>>>>>>>>>>>>>>>>>>>>>>> VERIFICATION <<<<<<<<<<<<<<<<<<<<<<<

const turnonGh1TurnOnBtn =   document.querySelector('.turnon-lightgh1-turnon-btn');
const turnonGh1CancelBtn =   document.querySelector('.turnon-lightgh1-cancel-btn');

const turnoffGh1TurnOffBtn =   document.querySelector('.turnoff-lightgh1-turnoff-btn');
const turnoffGh1CancelBtn =   document.querySelector('.turnoff-lightgh1-cancel-btn');

const turnOnLightDiv =  document.querySelector('.turnon-light-div');
const turnOffLightDiv = document.querySelector('.turnoff-light-div');

const gh1LightingOpenBtn   = document.querySelector('.gh1-lighting-open-btn');
const gh1LightingCloseBtn   = document.querySelector('.gh1-lighting-close-btn');
const gh1LightingModal = document.querySelector('.gh1-lighting-modal');


turnonGh1TurnOnBtn.addEventListener('click', onLighting)
turnonGh1CancelBtn.addEventListener('click', showVerifyDiv)
function showVerifyDiv(){
    turnOnLightDiv.classList.remove('show-verify');
}
turnoffGh1TurnOffBtn.addEventListener('click', onLighting2)
turnoffGh1CancelBtn.addEventListener('click', showVerifyDiv2)
function showVerifyDiv2(){
    turnOffLightDiv.classList.remove('show-verify');
}





const gh1LightingSwitch = document.querySelector('.gh1-lighting-switch');
const gh1LightingSubdiv = document.querySelector('.gh1-lighting-subdiv');


gh1LightingSwitch.addEventListener('click', gh1lightfunction)
function gh1lightfunction(){
    if (gh1LightingSubdiv.classList.contains('maingh-settings-off')){
        turnOnLightDiv.classList.toggle('show-verify');
    }
    else{
        turnOffLightDiv.classList.toggle('show-verify');
    }
    
}


function offLighting(){
    turnOnLightDiv.classList.toggle('show-verify');
    // gh1LightingSubdiv.classList.toggle('maingh-settings-off');
}
function onLighting(){
    turnOnLightDiv.classList.toggle('show-verify');
    gh1LightingSubdiv.classList.toggle('maingh-settings-off');
}
function onLighting2(){
    turnOffLightDiv.classList.toggle('show-verify');
    gh1LightingSubdiv.classList.toggle('maingh-settings-off');
}







gh1LightingOpenBtn.addEventListener('click', showLightingModal);
function showLightingModal(){
    gh1LightingModal.classList.add('view-modal');
}

gh1LightingCloseBtn.addEventListener('click', hideLightingModal);
function hideLightingModal(){
    gh1LightingModal.classList.remove('view-modal');
}
//LIGHTING





















const gh1WateringOpenBtn   = document.querySelector('.gh1-watering-open-btn');
const gh1WateringCloseBtn   = document.querySelector('.gh1-watering-close-btn');
const gh1WateringModal = document.querySelector('.gh1-watering-modal');

gh1WateringOpenBtn.addEventListener('click', showWateringModal);
function showWateringModal(){
    gh1WateringModal.classList.add('view-modal');
}

gh1WateringCloseBtn.addEventListener('click', hideWateringModal);
function hideWateringModal(){
    gh1WateringModal.classList.remove('view-modal');
}


const gh1WateringSwitch = document.querySelector('.gh1-watering-switch');
const gh1WateringSubdiv = document.querySelector('.gh1-watering-subdiv');

gh1WateringSwitch.addEventListener('click', offWatering)
function offWatering(){
    gh1WateringSubdiv.classList.toggle('maingh-settings-off');
}
//WATERING

const gh1VentilationOpenBtn   = document.querySelector('.gh1-ventilation-open-btn');
const gh1VentilationCloseBtn   = document.querySelector('.gh1-ventilation-close-btn');
const gh1VentilationModal = document.querySelector('.gh1-ventilation-modal');

gh1VentilationOpenBtn.addEventListener('click', showVentilationModal);
function showVentilationModal(){
    gh1VentilationModal.classList.add('view-modal');
}

gh1VentilationCloseBtn.addEventListener('click', hideVentilationModal);
function hideVentilationModal(){
    gh1VentilationModal.classList.remove('view-modal');
}


const gh1VentilationSwitch = document.querySelector('.gh1-ventilation-switch');
const gh1VentilationSubdiv = document.querySelector('.gh1-ventilation-subdiv');

gh1VentilationSwitch.addEventListener('click', offVentilation)
function offVentilation(){
    gh1VentilationSubdiv.classList.toggle('maingh-settings-off');
}
//VENTILATION

const gh1LogsOpenBtn   = document.querySelector('.gh1-logs-open-btn');
const gh1LogsCloseBtn   = document.querySelector('.gh1-logs-close-btn');
const gh1LogsModal = document.querySelector('.gh1-logs-modal');

gh1LogsOpenBtn.addEventListener('click', showLogsModal);
function showLogsModal(){
    gh1LogsModal.classList.add('view-modal');
}

gh1LogsCloseBtn.addEventListener('click', hideLogsModal);
function hideLogsModal(){
    gh1LogsModal.classList.remove('view-modal');
}
//LOGS



