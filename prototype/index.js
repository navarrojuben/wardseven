





const notifBtnOpen =    document.querySelector('.notif-btn-open');
const notifBtnClose =   document.querySelector('.notif-btn-close');
const notifDiv =   document.querySelector('.notif-div');

// const body         =   document.querySelector('body');

notifBtnOpen.addEventListener('click', openNotifDiv);
notifBtnClose.addEventListener('click', closeNotifDiv);

function openNotifDiv(){

    notifBtnOpen.classList.add('hide-notif-btn-open');
    notifBtnClose.classList.remove('hide-notif-btn-close');

    notifDiv.classList.add('open-notif-div');
    notifDiv.classList.remove('close-notif-div');

    // body.classList.add('no-scroll-body');

}

function closeNotifDiv(){
    
    notifBtnOpen.classList.remove('hide-notif-btn-open');
    notifBtnClose.classList.add('hide-notif-btn-close');

    notifDiv.classList.add('close-notif-div');

    // body.classList.remove('no-scroll-body');
}





// MAIN-MENU



const mainmenuBtnOpen =    document.querySelector('.mainmenu-btn-open');
const mainmenuBtnClose =   document.querySelector('.mainmenu-btn-close');
const mainmenuDiv =   document.querySelector('.mainmenu-div');
// const body         =   document.querySelector('body');

mainmenuBtnOpen.addEventListener('click', openMainmenuDiv);
mainmenuBtnClose.addEventListener('click', closeMainmenuDiv);

function openMainmenuDiv(){

    mainmenuBtnOpen.classList.add('hide-mainmenu-btn-open');
    mainmenuBtnClose.classList.remove('hide-mainmenu-btn-close');

    mainmenuDiv.classList.add('open-mainmenu-div');
    mainmenuDiv.classList.remove('close-mainmenu-div');

    // body.classList.add('no-scroll-body');

}

function closeMainmenuDiv(){
    
    mainmenuBtnOpen.classList.remove('hide-mainmenu-btn-open');
    mainmenuBtnClose.classList.add('hide-mainmenu-btn-close');

    mainmenuDiv.classList.add('close-mainmenu-div');

    // body.classList.remove('no-scroll-body');
}

const dropdownServicesBtn = document.querySelector('.dropdown-services-btn');
const dropdownServicesDiv = document.querySelector('.dropdown-services-div');

dropdownServicesBtn.addEventListener('click', showDropdownServices)

function showDropdownServices(){
        dropdownServicesDiv.classList.toggle('show-dropdown');
        console.log("shown");
}

const mainmenuBullets = document.querySelectorAll('.mainmenu-bullet-div');

mainmenuBullets.forEach(bullet => {
  bullet.addEventListener('click', closeMainmenuDiv);
});

const loginBtn = document.querySelector('.login-btn');
const loginPage = document.querySelector('.login-page');


loginBtn.addEventListener('click', hideMainPage);

function hideMainPage(){
    loginPage.classList.add('hide-login-page');
}





