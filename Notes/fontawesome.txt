home       - <i class="fa-solid fa-house"></i> 
validation - <i class="fa-solid fa-clipboard-check"></i>
contact    - <i class="fa-solid fa-address-card"></i>


light   - <i class="fa-solid fa-sun"></i>
water   - <i class="fa-solid fa-droplet"></i>
cooling - <i class="fa-solid fa-fan"></i>
logs    - <i class="fa-solid fa-clipboard"></i>


notifications - <i class="fa-solid fa-bell"></i>
others        - <i class="fa-solid fa-ellipsis-vertical"></i>
settings      - <i class="fa-solid fa-gear"></i>

profile - <i class="fa-solid fa-user"></i>
logout - <i class="fa-solid fa-right-from-bracket"></i>

tree - <i class="fa-solid fa-tree"></i>

leaf - <i class="fa-solid fa-seedling"></i>
