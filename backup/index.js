





const prototypeBtnOpen =    document.querySelector('.prototype-btn-open');
const prototypeBtnClose =   document.querySelector('.prototype-btn-close');
const prototypeDiv =   document.querySelector('.prototype-div');
// const body         =   document.querySelector('body');

prototypeBtnOpen.addEventListener('click', openPrototypeDiv);
prototypeBtnClose.addEventListener('click', closePrototypeDiv);

function openPrototypeDiv(){

    prototypeBtnOpen.classList.add('hide-prototype-btn-open');
    prototypeBtnClose.classList.remove('hide-prototype-btn-close');

    prototypeDiv.classList.add('open-prototype-div');
    prototypeDiv.classList.remove('close-prototype-div');

    // body.classList.add('no-scroll-body');

}

function closePrototypeDiv(){
    
    prototypeBtnOpen.classList.remove('hide-prototype-btn-open');
    prototypeBtnClose.classList.add('hide-prototype-btn-close');

    prototypeDiv.classList.add('close-prototype-div');

    // body.classList.remove('no-scroll-body');
}



const mainmenuBtnOpen =    document.querySelector('.mainmenu-btn-open');
const mainmenuBtnClose =   document.querySelector('.mainmenu-btn-close');
const mainmenuDiv =   document.querySelector('.mainmenu-div');
// const body         =   document.querySelector('body');

mainmenuBtnOpen.addEventListener('click', openMainmenuDiv);
mainmenuBtnClose.addEventListener('click', closeMainmenuDiv);

function openMainmenuDiv(){

    mainmenuBtnOpen.classList.add('hide-mainmenu-btn-open');
    mainmenuBtnClose.classList.remove('hide-mainmenu-btn-close');

    mainmenuDiv.classList.add('open-mainmenu-div');
    mainmenuDiv.classList.remove('close-mainmenu-div');

    // body.classList.add('no-scroll-body');

}

function closeMainmenuDiv(){
    
    mainmenuBtnOpen.classList.remove('hide-mainmenu-btn-open');
    mainmenuBtnClose.classList.add('hide-mainmenu-btn-close');

    mainmenuDiv.classList.add('close-mainmenu-div');

    // body.classList.remove('no-scroll-body');
}



const dropdownServicesBtn = document.querySelector('.dropdown-services-btn');
const dropdownServicesDiv = document.querySelector('.dropdown-services-div');

dropdownServicesBtn.addEventListener('click', showDropdownServices)

function showDropdownServices(){
        dropdownServicesDiv.classList.toggle('show-dropdown');
        console.log("shown");
}

const mainmenuBullets = document.querySelectorAll('.mainmenu-bullet-div');

mainmenuBullets.forEach(bullet => {
  bullet.addEventListener('click', closeMainmenuDiv);
});